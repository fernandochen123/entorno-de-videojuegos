using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(Animator))]
public class CharacterAnimBasedMovement : MonoBehaviour
{
    public float rotationSpeed = 4f;
    public float rotationTHreshold = 0.3f;
    [Range(0, 100f)]
    public float degreesToTurn = 160f;

    [Header("Animator Parameters")]
    public string motionParam = "Motion";
    public string mirrorIdleParam = "mirrorIdle";
    public string turn180param = " turn180";

    [Header("Animation smoothing")]
    [Range(0, 100f)]
    public float StarAnimTime = 0.3f;
    [Range(0, 1f)]
    public float StopAnimTime = 0.15f;

    private Ray wallray = new Ray();
    private float speed;
    private Vector3 desiredMoveDiRECTION;
    private CharacterController characterController;
    private Animator animator;
    private bool mirroIdle;
    private bool turn180;
    


    // Start is called before the first frame update
    void Start()
    {
        characterController = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();

       
    }

    public void movecharater(float hinput, float cinput , Camera cam , bool jump, bool dash)
    {
        speed = new Vector2(hinput, cinput).normalized.sqrMagnitude;

        if(speed >= speed - rotationTHreshold && dash)
        {
            speed = 0.5f;
        }
        if ( speed > rotationTHreshold)
        {
            animator.SetFloat(motionParam, speed, StarAnimTime, Time.deltaTime);
            Vector3 forward = cam.transform.forward;
            Vector3 right = cam.transform.right;

            forward.y = 0f;
            right.y = 0f;

            forward.Normalize();
            right.Normalize();

            desiredMoveDiRECTION = forward * cinput + right * hinput;
            if ( Vector3.Angle(transform.forward,desiredMoveDiRECTION) >= degreesToTurn)
            {
                turn180 = true;

            }
            else
            {
                turn180 = false;
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(desiredMoveDiRECTION), rotationSpeed * Time.deltaTime);
            }
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(desiredMoveDiRECTION), rotationSpeed * Time.deltaTime);
        }else if (speed < rotationTHreshold)
        {
            animator.SetBool(mirrorIdleParam, mirroIdle);
            animator.SetFloat(motionParam, speed, StopAnimTime, Time.deltaTime);
        }
    }

    private void OnAnimatorIK(int layerIndex)
    {
        if (speed < rotationTHreshold) return;

        float distantceToLeftFoot = Vector3.Distance(transform.position, animator.GetIKHintPosition(AvatarIKHint.LeftKnee));
        float distanceToRightFoot = Vector3.Distance(transform.position, animator.GetIKHintPosition(AvatarIKHint.RightKnee));

        if(distanceToRightFoot > distantceToLeftFoot)
        {
            mirroIdle = true;
        }else
        {
            mirroIdle = false;
        }
    }


    // Update is called once per frame
    void Update()
    {
        
    }
}
