using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IddleBehaviour : StateMachineBehaviour
{
    [SerializeField] private float _timeUntilNewIdle;
    [SerializeField] private int IddleID;
    private bool isSpecialIdle;
    [SerializeField] private float _idleTime;


    private int _specialAnim;


    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        resetIddle(animator);
    }


    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (isSpecialIdle)
        {
            _idleTime += Time.deltaTime;
            {
                if (_idleTime > _timeUntilNewIdle && stateInfo.normalizedTime % 1 < 0.02f)
                {
                    isSpecialIdle = true;
                    int specialAnim = Random.Range(1, IddleID + 1);
                    animator.SetFloat("_numberOfIdle", IddleID);
                }
            }
        }
        else if (stateInfo.normalizedTime % 1 > 0.98f)
        {
        }
    }

    private void resetIddle(Animator animator)
    {
        isSpecialIdle = false;
        _idleTime = 0;
        animator.SetFloat("_numberOfIddle", 0);
    }
}