using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTPSController : MonoBehaviour
{

    public Camera cam;
    private Imput input;
    private CharacterAnimBasedMovement characterMovement;
    // Start is called before the first frame update
    void Start()
    {
        characterMovement = GetComponent<CharacterAnimBasedMovement>();
    }

    // Update is called once per frame
    void Update()
    {
        input.getInput();

        characterMovement.movecharater(input.hmovement, input.vmovement, cam, input.jump, input.dash);
    }
}
